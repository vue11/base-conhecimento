import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({ // No main.js, quando faço import store from './config/store', significa que estou importando isso
    state: {
        isMenuVisible: false,
        user: null
    }, 
    mutations: {
        toggleMenu(state, isVisible) {
            if(!state.user) { // Se o usuario não estiver setado 
                state.isMenuVisible = false;
                return
            }

            if(isVisible === undefined) {
                state.isMenuVisible = !state.isMenuVisible;
            } else {
                state.isMenuVisible = isVisible;
            }
        },

        setUser(state, user) { // Recebo o state e o proprio usuario que eu quero setar na store
            state.user = user;

            if(user) { // Se o usuário foi setado
                axios.defaults.headers.common['Authorization'] = `Bearer ${user.token}` // Pego o token
                state.isMenuVisible = true; // Tenho um usuário válido então exibe o Menu
            } else { 
                delete axios.defaults.headers.common['Authorization'] // Excluo essa propriedade de dentro de common
                state.isMenuVisible = false; // Não deixo exibir o menu
            }

        }
    }
})
