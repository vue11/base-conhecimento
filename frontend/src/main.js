import 'font-awesome/css/font-awesome.css'
import Vue from 'vue'

import App from './App'

import store from './config/store'
import './config/bootstrap' // Não preciso passar o bootstrap para a instância do Vue, só faço carregar o arquivo de configuração
import router from './config/router'

Vue.config.productionTip = false

new Vue({
  store,  // Coloco aqui para o store ser compartilhado entre os componentes da minha aplicação
  router,
  render: h => h(App)
}).$mount('#app')